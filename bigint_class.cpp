#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <cstdio>

#define BASE 1000000000


using namespace std;


class BigInt{
    vector<unsigned int> bigInt;
    //true = negative
    bool sign;
public:
    BigInt(string number);
    BigInt(unsigned int number);
    string toString();
    vector<unsigned int>* getInternalRepr();
    void add(BigInt *other);
    void mul(BigInt *other);
    void addSimple(unsigned int other);
    void mulSimple(unsigned int other);



};


BigInt::BigInt(string number)
{
    //    first check for sign
    string signStr = number.substr(0,1);
    if(!signStr.compare("-"))
    {
        //negative
        this->sign = true;
        number = number.substr(1,number.length()-1);
    }
    //reverse string to read from the end
    number = string(number.rbegin(),number.rend());
    int len = number.length();
    for(int i=0;i<len;i+=9)
    {
        unsigned int parsedval;
        string val = number.substr(i,9);
        val = string(val.rbegin(),val.rend());
        stringstream ss(val);
        ss >> parsedval;
        this->bigInt.push_back(parsedval);
    }
}

string BigInt::toString()
{
    string result = "";
    char buf[9];
    bool first = true;
    vector<unsigned int>::reverse_iterator it(this->bigInt.rbegin());
    for(;it!=this->bigInt.rend();it++)
    {
        if(first)
        {
            sprintf(buf,"%d",*it);
            result.append(buf);
            first = false;
        }
        else
        {
            sprintf(buf,"%09d",*it);
            result.append(buf);
        }
    }
    return result;
}

vector<unsigned int>* BigInt::getInternalRepr()
{
    return &this->bigInt;
}

void BigInt::add(BigInt *other)
{

}

void BigInt::mul(BigInt *other)
{

}

void BigInt::addSimple(unsigned int other)
{
    int len = this->bigInt.size();
    int carry,i=0;
    unsigned long long sum;
    while(i < len)
    {
        sum = bigInt.at(i) + carry + other;
        bigInt.at(i) = sum % BASE;
        carry = sum / BASE;
        if(carry == 0)
            return;
        if(i==0)
            other = 0;
        i++;
    }
    if(carry>0)
        bigInt.push_back(carry);
}

void BigInt::mulSimple(unsigned int other)
{

}



int main()
{
    string read;
    getline(cin,read);
    BigInt *bInt = new BigInt(read);
    bInt->addSimple(123456);
    cout << bInt->toString() << endl;

    return 0;
}
